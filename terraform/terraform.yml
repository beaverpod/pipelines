# Based on https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Terraform.latest.gitlab-ci.yml.

default:
  image:
    name: hashicorp/terraform:1.1.2
    entrypoint: [""]
  before_script:
    - export TF_HTTP_ADDRESS=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME:-main}
    - export TF_HTTP_LOCK_ADDRESS=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME:-main}/lock
    - export TF_HTTP_UNLOCK_ADDRESS=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME:-main}/lock
  cache:
    key: "${TF_ROOT}"
    paths:
      - ${TF_ROOT}/.terraform/

stages:
  - validate
  - build
  - deploy
  - cleanup

variables:
  TF_ROOT: ${CI_PROJECT_DIR}/terraform   # The relative path to the root directory of the Terraform project
  TF_STATE_NAME: main
  TF_HTTP_UPDATE_METHOD: POST
  TF_HTTP_LOCK_METHOD: POST
  TF_HTTP_UNLOCK_METHOD: DELETE
  TF_HTTP_USERNAME: gitlab-ci-token
  TF_HTTP_PASSWORD: ${CI_JOB_TOKEN}
  TF_HTTP_RETRY_WAIT_MIN: 5
  TF_HTTP_RETRY_WAIT_MAX: 30
  TF_IN_AUTOMATION: "true"

fmt:
  stage: validate
  needs: []
  script:
    - cd ${TF_ROOT}
    - terraform fmt -check -diff -recursive
  allow_failure: true

validate:
  stage: validate
  needs: []
  script:
    - cd ${TF_ROOT}
    - terraform init -input=false -backend=false
    - terraform validate

build:
  stage: build
  variables:
    JQ_PLAN: |
      (
        [.resource_changes[]?.change.actions?] | flatten
      ) | {
        "create":(map(select(.=="create")) | length),
        "update":(map(select(.=="update")) | length),
        "delete":(map(select(.=="delete")) | length)
      }

  script:
    - apk add --no-cache jq
    - cd ${TF_ROOT}
    - terraform init -input=false
    - terraform plan -input=false -out=plan.cache
    - terraform show -json plan.cache | jq -r "${JQ_PLAN}" > plan.json
  resource_group: ${TF_STATE_NAME}
  artifacts:
    paths:
      - ${TF_ROOT}/plan.cache
    reports:
      terraform: ${TF_ROOT}/plan.json

deploy:
  stage: deploy
  script:
    - cd ${TF_ROOT}
    - terraform init -input=false
    - terraform apply -input=false plan.cache
  resource_group: ${TF_STATE_NAME}
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual
  dependencies:
    - build
  environment:
    name: $TF_STATE_NAME

destroy:
  stage: cleanup
  script:
    - cd ${TF_ROOT}
    - terraform init -input=false
    - terraform destroy -auto-approve
  resource_group: ${TF_STATE_NAME}
  when: manual
